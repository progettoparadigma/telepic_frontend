import { Component, OnInit } from '@angular/core';
import { PopoverController, NavParams, NavController } from '@ionic/angular'
import { Creator } from 'src/app/interfaces/user';
import { UsersService } from 'src/app/services/users/users.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoaderService } from 'src/app/shared/loader.service';
import { ToastService } from 'src/app/shared/toast.service';
import { AlertService } from 'src/app/shared/alert.service';
import { ToastTypes } from 'src/app/enums/toast-types.enum';

@Component({
  selector: 'app-profile-popover',
  templateUrl: './profile-popover.component.html',
  styleUrls: ['./profile-popover.component.scss'],
})
export class ProfilePopoverComponent implements OnInit {

  me: Creator;
  editMode: boolean;

  constructor(
    public popoverController: PopoverController,
    private navParams: NavParams,
    private usersService: UsersService,
    private auth: AuthService,
    private navCtrl: NavController,
    private loader: LoaderService,
    private toastService: ToastService,
    private alert: AlertService,
  ) { }

  ngOnInit() {
    this.me = this.navParams.get('myProfile');
    this.editMode = this.navParams.get('editMode');
  }

  delete(){
    this.deleteMe();
    this.popoverController.dismiss();
  }

  async deleteMe() {

    const alertButtons = [{
      text: 'Cancel',
      role: 'cancel',
      cssClass: 'secondary',
      handler: () => {}
    }, {
      text: 'OK',
      handler: async () => {
        this.confirmedDeletedUser();
      }
    }];

    await this.alert.show({
      header: 'Wait',
      subHeader: 'You are about to delete your own account.',
      message: 'Are you sure that you want to proceed with this action?',
      buttons: alertButtons
    });

  }

  async confirmedDeletedUser() {

    try {

      await this.loader.show();
      await this.usersService.deleteUser(this.me);
      await this.logout();

    } catch (error) {

      await this.toastService.show({
        message: error.message,
        type: ToastTypes.ERROR

      });

    }

    await this.loader.dismiss();

  }

  async logout() {

    this.auth.me = {} as Creator;
    this.auth.userToken = undefined;
    await this.navCtrl.navigateRoot('/login');
    this.popoverController.dismiss();
  }

}
