import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Topic, NewTopic } from 'src/app/interfaces/topic';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';
import { Message } from 'src/app/interfaces/message';


@Injectable({
  providedIn: 'root'
})
export class TopicsService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
    })
  }

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) { }

  async createTopic(newTopic: NewTopic){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.post<Topic>(`${environment.API_URL}/topics/`, newTopic, {
      headers: headerOptions
    }).toPromise();  
  }

  async subscribeTopic(topic: Topic){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.put<Topic>(`${environment.API_URL}/topics/join/${ topic._id }`, topic, {
      headers: headerOptions
    }).toPromise();  
  }

  async unSubscribeTopic(topic: Topic){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.put<Topic>(`${environment.API_URL}/topics/unsub/${ topic._id }`, topic, {
      headers: headerOptions
    }).toPromise();  
  }

  async getMyCreatedTopic(){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.get<Topic[]>(`${environment.API_URL}/topics/created`, {
      headers: headerOptions
    }).toPromise();
  }

  async getMyJoinedTopic(){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.get<Topic[]>(`${environment.API_URL}/topics/joined`, {
      headers: headerOptions
    }).toPromise();
  }

  async getMyTopic(){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.get<Topic[]>(`${environment.API_URL}/topics/home`, {
      headers: headerOptions
    }).toPromise();
  }

  async searchMyTopics(query: string){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.get<Topic[]>(`${environment.API_URL}/topics/home/search?name=${query}`, {
      headers: headerOptions
    }).toPromise();
  }

  async searchExploreTopics(query: string){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.get<Topic[]>(`${environment.API_URL}/topics/explore/search?name=${query}`, {
      headers: headerOptions
    }).toPromise();
  }

  async getTopic(topic : Topic){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.get<Topic>(`${environment.API_URL}/topics/topic/${ topic._id }`, {
      headers: headerOptions
    }).toPromise();    
  }

  async getTopics(){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.get<Topic[]>(`${environment.API_URL}/topics/`, {
      headers: headerOptions
    }).toPromise();    
  }

  async getNotJoinedTopics(){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.get<Topic[]>(`${environment.API_URL}/topics/explore`, {
      headers: headerOptions
    }).toPromise();    
  }

  async editTopic(topic : Topic){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.put<Topic[]>(`${environment.API_URL}/topics/topic/${ topic._id }`, topic, {
      headers: headerOptions
    }).toPromise();    
  }

  async deleteTopic(topic : Topic){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.delete<Topic[]>(`${environment.API_URL}/topics/topic/${ topic._id }`, {
      headers: headerOptions
    }).toPromise();    
  }

  async getMessages(topic : Topic){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.get<Message[]>(`${environment.API_URL}/messages/topic/${ topic._id }`, {
      headers: headerOptions
    }).toPromise();   
  }

  async sendMessage(topic: Topic, message: Message){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.post<Message[]>(`${environment.API_URL}/messages/topic/${ topic._id }`, message, {
      headers: headerOptions
    }).toPromise();  
  }

}
