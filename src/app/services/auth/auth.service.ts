import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Login, LoginResponse} from 'src/app/interfaces/login';
import { User, Creator } from 'src/app/interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  public userToken: string;
  public me = {} as Creator;

  constructor(
    private http: HttpClient
  ) { }

  async login(body: Login): Promise<void> {

    return new Promise<void>((resolve, reject) => {
      const sub = this.http.post<LoginResponse>(`${environment.API_URL}/login`, body, this.httpOptions)
      .subscribe(async res => {
        this.userToken = res.authtoken;
        sub.unsubscribe();
        this.me = await this.getMe();
        resolve();
      }, reject);

    });

  }

    async getMe() {
      const headerOptions = this.httpOptions.headers.append('authtoken', `${this.userToken}`);
      return this.http.get<Creator>(`${environment.API_URL}/me`, {
        headers: headerOptions
      }).toPromise();
    }
}
