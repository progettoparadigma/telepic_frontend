import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User, Creator } from 'src/app/interfaces/user';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor( 
    private http: HttpClient,
    private auth: AuthService
  ) {}

  //Creazione
  async createUser(user : User){
    return this.http.post<User>(`${environment.API_URL}/users/`,user, this.httpOptions).toPromise();
  }

  async editUser(user: Creator){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.put<Creator[]>(`${environment.API_URL}/users/${ user._id }`, user, {
      headers: headerOptions
    }).toPromise();    
  }

  async deleteUser(user: Creator){
    const headerOptions = this.httpOptions.headers.append('authtoken', `${this.auth.userToken}`);

    return this.http.delete<Creator[]>(`${environment.API_URL}/users/${ user._id }`, {
      headers: headerOptions
    }).toPromise();    
  }
}
