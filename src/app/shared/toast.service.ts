import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ToastTypes } from 'src/app/enums/toast-types.enum';
import { ToastOptions } from '../interfaces/toast-options';

@Injectable()
export class ToastService {

    constructor(private toastCtrl: ToastController) {}

    async show(options: ToastOptions) {

        if (!options.duration) { options.duration = 5000; }
        if (!options.type) { options.type = ToastTypes.WARNING; }
        if (!options.position) { options.position = 'top'; }

        const toast = await this.toastCtrl.create(
        {
            message: options.message,
            cssClass: options.type,
            duration: options.duration,
            position: options.position,
        });

        return toast.present();

    }
}
