import { Component, OnInit } from '@angular/core';
import { ToastService } from 'src/app/shared/toast.service';
import { Topic } from 'src/app/interfaces/topic';
import { TopicsService } from 'src/app/services/topics/topics.service';
import { ToastTypes } from 'src/app/enums/toast-types.enum';
import { AuthService } from 'src/app/services/auth/auth.service';
import { NavparamService } from 'src/app/services/navparam/navparam.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-explore',
  templateUrl: './explore.page.html',
  styleUrls: ['./explore.page.scss'],
})
export class ExplorePage implements OnInit {

  topics: Topic[] = [];
  isEmpty: boolean;
  isSub: boolean[] = [false];
  search: string = "";

  constructor(
    private toastService: ToastService,
    private topicService: TopicsService,
    private navparamService:NavparamService,
    private router: Router
  ) { }

  async ngOnInit() {
    await this.getTopics();
  }

  async getTopics(){
    try {

          this.topics = await this.topicService.getNotJoinedTopics();
          
    } catch (error) {
        await this.toastService.show({
        message: error.message,
        type: ToastTypes.ERROR
      });    
    }

    this.isEmpty = this.topics.length == 0;
  }

  async searchTopics(event){
    if(event == 13){
      try {
        this.topics = await this.topicService.searchExploreTopics(this.search);
        if(this.topics.length == 0 && this.search.length!=0){
          await this.toastService.show({
            message: "Nothing to see, sorry",
            position: 'bottom',
            type: ToastTypes.WARNING
          }); 
        }
      } catch (error) {
        await this.toastService.show({
          message: error.message,
          type: ToastTypes.ERROR
        }); 
      }
    }
    if(this.search.length == 0){
      await this.getTopics();
    }
  }

  async enterTopic(selectedTopic: Topic){
    this.navparamService.setNavData(selectedTopic);
    await this.router.navigate(['topic']);
  }

  async subscribeTopic(topic: Topic, i: number){
    try {
      
      await this.topicService.subscribeTopic(topic);
      this.isSub[i] = true;

    } catch (error) {
        await this.toastService.show({
        message: error.message,
        type: ToastTypes.ERROR
      });  
    }

  }

  async unSubscribeTopic(topic: Topic, i: number){
    try {
      
      await this.topicService.unSubscribeTopic(topic);
      this.isSub[i] = false;

    } catch (error) {
        await this.toastService.show({
        message: error.message,
        type: ToastTypes.ERROR
      });  
    }

  }

  async refresh(event){
    this.isSub = [false];
    await this.getTopics();
    setTimeout(()=> {
      event.target.complete();
    },1500);
    
  }

  getAuthor(topic: Topic): string{
    if(topic._creator){
      return topic._creator.nickname;
    }
    else{
      return "User Deleted";
    }
  }

}