import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { TopicsService } from 'src/app/services/topics/topics.service';
import { ToastService } from 'src/app/shared/toast.service';
import { LoaderService } from 'src/app/shared/loader.service';
import { Topic, NewTopic } from 'src/app/interfaces/topic';
import { ToastTypes } from 'src/app/enums/toast-types.enum';

@Component({
  selector: 'app-new-topic',
  templateUrl: './new-topic.page.html',
  styleUrls: ['./new-topic.page.scss'],
})
export class NewTopicPage implements OnInit {

  newTopic = {} as NewTopic;

  topicToEdit: Topic;
  editMode = false;

  topicsPics: number[] = [1,2,3,4,5,6,7,8,9,10,11,12,13];
  private iconFocussed = 1;

  className: string[] = ["normal"];

  constructor(
    private modalCtrl: ModalController,
    private topicsService: TopicsService,
    private navParams: NavParams,
    private toastService: ToastService,
    private loader: LoaderService
  ) { }

  ngOnInit() {
    this.topicToEdit = this.navParams.get('topic');
    this.editMode = this.topicToEdit !== undefined;

    if(this.editMode){
      this.iconFocussed = (Number) (this.topicToEdit.iconName.substring(0,this.topicToEdit.iconName.length-4));
    }
    this.className[this.iconFocussed-1] = "focusIcon";
  }

  changeClass(iter){
    this.className = ["normal"];
    this.className[iter-1] = "focusIcon";
    this.iconFocussed = iter;
  }

  isNameInvalid() : boolean{
    if(this.editMode){
      if(this.topicToEdit.name){
        return this.topicToEdit.name.length < 4;
      }
    }
    else if(!this.editMode){
      if(this.newTopic.name){
        return this.newTopic.name.length < 4;
      }
    }
    return true;
  }

  async dismiss(){
    await this.modalCtrl.dismiss();
  }

  async createOrEditTopic(){

    try {
      
      await this.loader.show();

      if(this.editMode){
        console.log("prima " + this.topicToEdit.iconName);
        this.topicToEdit.iconName = this.iconFocussed + ".svg";
        console.log("dopo" + this.topicToEdit.iconName);
        console.log("focussed" + this.iconFocussed);
        await this.topicsService.editTopic(this.topicToEdit);

        await this.toastService.show({
          message: "Topic Editato con Successo",
          type: ToastTypes.SUCCESS
        });

      } else {
        this.newTopic.iconName = this.iconFocussed + ".svg";
        await this.topicsService.createTopic(this.newTopic);
        
        await this.toastService.show({
          message: "Topic Creato con Successo",
          type: ToastTypes.SUCCESS
        });
      }

      await this.loader.dismiss();
      
      await this.dismiss();



    } catch (error) {
      await this.toastService.show({
        message: error.message,
        type: ToastTypes.ERROR
      });
    }
  }
}
