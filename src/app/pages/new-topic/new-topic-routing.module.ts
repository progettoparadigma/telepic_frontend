import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewTopicPage } from './new-topic.page';

const routes: Routes = [
  {
    path: '',
    component: NewTopicPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewTopicPageRoutingModule {}
