import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewTopicPage } from './new-topic.page';

describe('NewTopicPage', () => {
  let component: NewTopicPage;
  let fixture: ComponentFixture<NewTopicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewTopicPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewTopicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
