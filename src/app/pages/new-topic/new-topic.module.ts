import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewTopicPageRoutingModule } from './new-topic-routing.module';

import { NewTopicPage } from './new-topic.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewTopicPageRoutingModule
  ],
  declarations: [NewTopicPage]
})
export class NewTopicPageModule {}
