import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {

  tabPosition: string = "bottom";

  constructor(
    private platform: Platform 
  ) {
    if(!this.platform.is("mobile")){
      this.tabPosition = "top";
    }
  }

  ngOnInit() {
  }

}
