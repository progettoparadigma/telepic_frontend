import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { UsersService } from 'src/app/services/users/users.service'
import { NavController, ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Login } from 'src/app/interfaces/login';
import { ToastService } from 'src/app/shared/toast.service';
import { ToastTypes } from 'src/app/enums/toast-types.enum';
import { LoaderService } from 'src/app/shared/loader.service';
import { UserImagesPage } from '../user-images/user-images.page';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  userSignUp = {} as User;
  userIndex = 1;

  constructor(
    private navCtrl: NavController,
    private usersService: UsersService,
    private auth: AuthService,
    private toastService: ToastService,
    private loader: LoaderService,
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.userSignUp.iconName = "1.svg";
  }

  async selectIcon(){
    const modal = await this.modalCtrl.create({
      component: UserImagesPage,
      componentProps:{
        user: this.userSignUp,
        isNew: true
      }
    });

    return await modal.present();
  }

  async signup(){

    try {

      await this.loader.show();
      
      await this.usersService.createUser(this.userSignUp);

      const loginUser: Login = {
        nickname: this.userSignUp.nickname,
        password: this.userSignUp.password
      }

      await this.auth.login(loginUser);

      await this.navCtrl.navigateRoot('/');

      await this.toastService.show({
        message: 'success',
        duration: 1000,
        type: ToastTypes.SUCCESS
      });

    } catch (error) {
      await this.toastService.show({
        message: error.message,
        type: ToastTypes.WARNING
      });
    }

    await this.loader.dismiss();
  }

  async goOnLogin(){
    await this.navCtrl.navigateRoot('/login');
  }

  isNicknameInvalid() : boolean{
    if(this.userSignUp.nickname){
      return this.userSignUp.nickname.length < 4;
    }
    return true;
  }

  isEmailInvalid() : boolean{
    if(this.userSignUp.email){
      return this.userSignUp.email.length < 5;
    }
    return true;
  }

  isPswInvalid() : boolean{
    if(this.userSignUp.password){
      return this.userSignUp.password.length < 8;
    }
    return true;
  }

  isAllInvalid(): boolean{
    if(!this.isPswInvalid() && !this.isNicknameInvalid() && !this.isEmailInvalid()){
      return false;
    }
    return true;
  }


}
