import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserImagesPage } from './user-images.page';

describe('UserImagesPage', () => {
  let component: UserImagesPage;
  let fixture: ComponentFixture<UserImagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserImagesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserImagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
