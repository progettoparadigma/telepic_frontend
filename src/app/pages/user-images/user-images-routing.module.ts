import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserImagesPage } from './user-images.page';

const routes: Routes = [
  {
    path: '',
    component: UserImagesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserImagesPageRoutingModule {}
