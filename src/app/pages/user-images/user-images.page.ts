import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Creator, User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-user-images',
  templateUrl: './user-images.page.html',
  styleUrls: ['./user-images.page.scss'],
})
export class UserImagesPage implements OnInit {

  me: Creator;
  newMe: User;
  isNew: boolean;

  usersPics: number[] = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];
  private iconFocussed = 1;

  className: string[] = ["normal"];

  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams
  ) { }

  ngOnInit() {

    this.isNew = this.navParams.get("isNew");
    
    if(this.isNew){
      this.newMe = this.navParams.get("user");
    }
    else
        {
          this.me = this.navParams.get("user");
          this.iconFocussed = (Number) (this.me.iconName.substring(0,this.me.iconName.length-4));
        }
    
        this.className[this.iconFocussed-1] = "focusIcon";
  }

  changeClass(iter){
    this.className = ["normal"];
    this.className[iter-1] = "focusIcon";
    this.iconFocussed = iter;
  }

  changeAndExit(){
    if(this.isNew){
      this.newMe.iconName = this.iconFocussed + ".svg";
    }
    else{
      this.me.iconName = this.iconFocussed + ".svg";
    }
    this.dismiss();
  }

  async dismiss(){
    await this.modalCtrl.dismiss();
  }

}
