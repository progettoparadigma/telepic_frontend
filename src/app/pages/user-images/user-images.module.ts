import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserImagesPageRoutingModule } from './user-images-routing.module';

import { UserImagesPage } from './user-images.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserImagesPageRoutingModule
  ],
  declarations: [UserImagesPage]
})
export class UserImagesPageModule {}
