import { Component, OnInit } from '@angular/core';
import { Login } from 'src/app/interfaces/login';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ToastService } from 'src/app/shared/toast.service';
import { ToastTypes } from 'src/app/enums/toast-types.enum';
import { LoaderService } from 'src/app/shared/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  userCredentials = {} as Login;

  constructor(
    private navCtrl : NavController,
    private auth: AuthService,
    private toastService: ToastService,
    private loader: LoaderService
  ) { }

  ngOnInit() {
  }

  async goOnSignup(){
    await this.navCtrl.navigateRoot('/signup');
  }

  async login(){

    try {

      await this.loader.show();

      await this.auth.login(this.userCredentials);

      await this.navCtrl.navigateRoot('/');

      await this.toastService.show({
        message: 'welcome back ' + this.userCredentials.nickname,
        duration: 1000,
        type: ToastTypes.SUCCESS
      });

    } catch (error) {

      await this.toastService.show({
        message: error.message,
        type: ToastTypes.ERROR
      });
    }

    await this.loader.dismiss();

  }

  isNicknameInvalid() : boolean{
    if(this.userCredentials.nickname){
      return this.userCredentials.nickname.length < 4;
    }
    return true;
  }

  isPswInvalid() : boolean{
    if(this.userCredentials.password){
      return this.userCredentials.password.length < 8;
    }
    return true;
  }

  isAllInvalid(): boolean{
    if(!this.isPswInvalid() && !this.isNicknameInvalid()){
      return false;
    }
    return true;
  }
}
