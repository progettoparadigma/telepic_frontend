import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UsersService } from 'src/app/services/users/users.service';
import { Creator } from 'src/app/interfaces/user';
import { NavController, ModalController } from '@ionic/angular';
import { LoaderService } from 'src/app/shared/loader.service';
import { ToastService } from 'src/app/shared/toast.service';
import { ToastTypes } from 'src/app/enums/toast-types.enum';
import { AlertService } from 'src/app/shared/alert.service';
import { PopoverController } from '@ionic/angular';
import { ProfilePopoverComponent } from 'src/app/components/profile-popover/profile-popover.component';
import { UserImagesPage } from '../user-images/user-images.page';
import { TopicsService } from 'src/app/services/topics/topics.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  me: Creator;

  readonly = true;

  topicsCreated: number;
  topicsJoined: number;

  constructor(
    private auth: AuthService,
    private usersService: UsersService,
    private navCtrl: NavController,
    private loader: LoaderService,
    private toastService: ToastService,
    private alert: AlertService,
    public popoverController: PopoverController,
    public modalCtrl: ModalController,
    private topicsService: TopicsService
  ) { }

  async ngOnInit() {
    await this.refresh();
  }

  async refresh(){
    this.me = this.auth.me;
    this.topicsCreated = (await this.topicsService.getMyCreatedTopic()).length;
    this.topicsJoined = (await this.topicsService.getMyJoinedTopic()).length;
  }

  async presentPopover(event){
    const popover = await this.popoverController.create({
      component: ProfilePopoverComponent,
      event,
      componentProps: {
        myProfile: this.me,
        editMode: this.readonly
      }
    });
    return await popover.present();
  }

  async editAndSave() {

    if (!this.readonly) {
      try {

        await this.loader.show();

        await this.usersService.editUser(this.me);

        await this.loader.dismiss();

        await this.toastService.show({
          message: 'Your account edits are now safe and sound!',
          type: ToastTypes.SUCCESS
        });

      } catch (err) {

        await this.loader.dismiss();

        await this.toastService.show({
          message: err.message,
          type: ToastTypes.ERROR
        });

      }

      await this.refresh();
    }

    this.readonly = !this.readonly;

  }

  async deleteMe() {

    const alertButtons = [{
      text: 'Cancel',
      role: 'cancel',
      cssClass: 'secondary',
      handler: () => {}
    }, {
      text: 'OK',
      handler: async () => {
        this.confirmedDeletedUser();
      }
    }];

    await this.alert.show({
      header: 'Wait',
      subHeader: 'You are about to delete your own account.',
      message: 'Are you sure that you want to proceed with this action?',
      buttons: alertButtons
    });

  }

  async confirmedDeletedUser() {

    try {

      await this.loader.show();
      await this.usersService.deleteUser(this.me);
      await this.logout();

    } catch (err) {

      await this.toastService.show({
        message: err.message,
        type: ToastTypes.ERROR

      });

    }

    await this.loader.dismiss();

  }

  async logout() {

    this.auth.me = {} as Creator;
    this.auth.userToken = undefined;
    await this.navCtrl.navigateRoot('/login');

  }

  async changeIcon(){
    const modal = await this.modalCtrl.create({
      component: UserImagesPage,
      componentProps:{
        user: this.me,
        isNew: false
      }
    });

    modal.onDidDismiss().then(async () => {
      
      await this.refresh();

    });

    return await modal.present();
  }

  async refreshInfo(event){
    
    await this.refresh();
    setTimeout(()=> {
      event.target.complete();
    },1500);
    
  }

  async ionViewWillEnter(){
    await this.refresh();
  }
}