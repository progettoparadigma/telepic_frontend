import { Component, OnInit } from '@angular/core';
import { Topic } from 'src/app/interfaces/topic'
import { LoaderService } from 'src/app/shared/loader.service';
import { ToastService } from 'src/app/shared/toast.service';
import { ModalController } from '@ionic/angular';
import { NewTopicPage } from '../new-topic/new-topic.page';
import { TopicsService } from 'src/app/services/topics/topics.service';
import { ToastTypes } from 'src/app/enums/toast-types.enum';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { NavparamService } from 'src/app/services/navparam/navparam.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  topics: Topic[] = [];
  isEmpty: boolean;
  search: string = "";

  constructor(
    private loader: LoaderService,
    private toastService: ToastService,
    private modalCtrl: ModalController,
    private topicService: TopicsService,
    private navparamService:NavparamService,
    private auth: AuthService,
    private router: Router
  ) { }


  printTopic(){
    console.log("NUMBER OF TOPIC " + this.topics.length + "\n")
    this.topics.forEach(element => {
      console.log("name " + element.name + "\n");
    });
  }

  async ngOnInit() {
    await this.getTopics();
  }

  async enterTopic(selectedTopic: Topic){
    this.navparamService.setNavData(selectedTopic);
    await this.router.navigate(['topic']);
  }

  async createOrEditTopic(topic?: Topic){

    const modal = await this.modalCtrl.create({
      component: NewTopicPage,
      componentProps:{
        topic
      }
    });

    modal.onDidDismiss().then(async () => {
      
      await this.getTopics();

    });

    return await modal.present();
  }

  async getTopics(){

    try {
      
      this.topics = await this.topicService.getMyTopic();

    } catch (error) {
      await this.toastService.show({
        message: error.message,
        type: ToastTypes.ERROR
      });
    }
    this.isEmpty = this.topics.length == 0;
    this.printTopic();
  }

  async searchTopics(event){
    if(event == 13){
      try {
        this.topics = await this.topicService.searchMyTopics(this.search);
        if(this.topics.length == 0 && this.search.length!=0){
          await this.toastService.show({
            message: "Nothing to see, sorry",
            position: 'bottom',
            type: ToastTypes.WARNING
          }); 
        }
      } catch (error) {
        await this.toastService.show({
          message: error.message,
          type: ToastTypes.ERROR
        }); 
      }
    }
    if(this.search.length == 0){
      await this.getTopics();
    }
  }

  canEdit(topic: Topic): boolean{
    if(topic._creator){
      return topic._creator._id === this.auth.me._id;
    }
    return false;
  }

  getAuthor(topic: Topic): string{
    if(this.canEdit(topic)){
      return "me";
    }
    else if(topic._creator){
      return topic._creator.nickname;
    }
    else{
      return "User Deleted";
    }
  }

  async refresh(event){
    
    await this.getTopics();
    setTimeout(()=> {
      event.target.complete();
    },1500);
    
  }

  async delete(topic: Topic){
    try {
      this.topicService.deleteTopic(topic);
      this.toastService.show({
        message: "Topic successfully deleted",
        type: ToastTypes.SUCCESS
      });
    } catch (error) {
      this.toastService.show({
        message: error.message,
        type: ToastTypes.ERROR
      });
    }
    await this.getTopics();
  }

  async unSubscribe(topic: Topic){
    try {
      this.topicService.unSubscribeTopic(topic);
      this.toastService.show({
        message: "Successfully unsubscribed",
        type: ToastTypes.SUCCESS
      });
    } catch (error) {
      this.toastService.show({
        message: error.message,
        type: ToastTypes.ERROR
      });
    }
    await this.getTopics();
  }

  ionViewWillEnter(){
    this.getTopics();
  }

}
