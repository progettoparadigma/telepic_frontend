import { Component, OnInit, ViewChild } from '@angular/core';
import { NavparamService } from 'src/app/services/navparam/navparam.service';
import { TopicsService } from 'src/app/services/topics/topics.service';
import { Topic } from 'src/app/interfaces/topic';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Message } from 'src/app/interfaces/message';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.page.html',
  styleUrls: ['./topic.page.scss'],
})
export class TopicPage implements OnInit {

  mex = {} as Message;
  topic: Topic;
  isNotSubscribed: boolean = false;
  messages: Message[];
  @ViewChild("content") content: IonContent;
  
  constructor(
    private navparamService: NavparamService,
    private topicsService: TopicsService,
    private auth: AuthService
  ) { }

  async ngOnInit() {
    this.topic = this.navparamService.getNavData();
    this.topic = await this.topicsService.getTopic(this.topic);
    this.refresh();
  }

  async send(event){
    if(event == 13 && this.mex.text.trim()!=""){
      await this.topicsService.sendMessage(this.topic, this.mex);
      this.mex.text = "";
      await this.refresh();
    }
  }

  isMyMessage(message: Message){
    return message._creator._id == this.auth.me._id;
  }

  isSubscribed(){
    return this.topic.joined.includes(this.auth.me._id) || this.topic._creator._id == this.auth.me._id;
  }

  async refresh(){
    this.messages = await this.topicsService.getMessages(this.topic);
    //this.content.scrollToBottom(300);
  }

}
