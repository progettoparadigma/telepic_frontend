import { Creator } from './user';

export interface Message {
    created_at: Date,
    _id: string,
    text: string,
    _topicId: string,
    _creator: Creator
}
