import { ToastTypes } from '../enums/toast-types.enum';

export interface ToastOptions {
        message?: string;
        type?: ToastTypes;
        duration?: number;
        accountDisabled?: boolean;
        position?: any;
}