import { Creator, User } from './user';

export interface NewTopic {
    name: string,
    iconName: string
}

export interface Topic {
    created_at: Date,
    joined: string[],
    iconName: string,
    _id: string,
    name: string,
    _creator: Creator,
    __v: number
}
