export interface Login {
    nickname: string;
    password: string;
}

export interface LoginResponse {
    authtoken: string;
    expiresIn: number;
}
