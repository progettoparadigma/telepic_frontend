export interface User {
    iconName : string;
    nickname: string;
    email: string;
    password: string;
}

export interface Creator{
    _id: string;
    iconName : string;
    nickname: string;
    email: string;
    __v: number;
}
