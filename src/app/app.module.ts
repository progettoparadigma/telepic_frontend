import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ToastService } from './shared/toast.service';
import { LoaderService } from './shared/loader.service';
import { NewTopicPageModule } from './pages/new-topic/new-topic.module';
import { AlertService } from './shared/alert.service';
import { NavparamService } from './services/navparam/navparam.service';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule, 
    HttpClientModule, 
    NewTopicPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ToastService,
    LoaderService,
    AlertService,
    NavparamService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
