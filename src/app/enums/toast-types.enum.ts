export enum ToastTypes {
    ERROR = 'error',
    SUCCESS = 'success',
    WARNING = 'warning',
    GENERAL = 'general',
    IN_APP = 'in_app'
}